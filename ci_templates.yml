############################################################
#####              BUILD IMAGES                        #####
############################################################

# Build the docker images
.template_build_kaniko_image:
  image: # NB enable shared runners and do not specify a CI tag
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder # CERN version of the Kaniko image
    entrypoint: [""]
  script:
    - echo "current commit is ${CI_COMMIT_SHORT_SHA}"
    - echo "current branch is ${CI_COMMIT_BRANCH}"
    - echo "current tag is ${CI_COMMIT_TAG}"
    - echo "DESTINATIONS $DESTINATIONS"
    - if [[ -z "$DOCKERFILE" ]]; then echo "ERROR variable DOCKERFILE is not defined "; exit 1; fi 
    - if [[ -z "$CONTEXT" ]]; then echo "ERROR variable CONTEXT is not defined "; exit 1; fi 
    - if [[ -z "$DESTINATIONS" ]]; then echo "ERROR variable DESTINATIONS is not defined "; exit 1; fi 
    # Prepare Kaniko configuration file
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # Build and push the image from the Dockerfile at the root of the project.
    # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
    # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
    - /kaniko/executor --context $CONTEXT --dockerfile $DOCKERFILE $DESTINATIONS 
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa-.*$/
      - $CI_COMMIT_BRANCH =~ /^qa$/
      - $CI_COMMIT_TAG =~ /^v.*$/

###########################################################
# use docker in docker image: to trigger other docker runs
###########################################################
.template_build_image:
    tags:
        - hep-workload-gpu-docker-builder
    image: 
        name: gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-builder/dind:qa # Use instead of kaniko. FIXME use a prod tag
        entrypoint: [""]
    script:
        - echo "current commit is ${CI_COMMIT_SHA:0:8}"
        - echo "current branch is ${CI_COMMIT_BRANCH}"
        - echo "current tag is ${CI_COMMIT_TAG}"
        - if [[ -z $DOCKERFILE ]]; then echo "ERROR variable DOCKERFILE is not defined "; exit 1; fi 
        - if [[ -z $CONTEXT ]]; then echo "ERROR variable CONTEXT is not defined "; exit 1; fi 
        - if [[ -z $IMAGE_NAME ]]; then echo "ERROR variable IMAGE_NAME is not defined "; exit 1; fi 
        - if [[ -z $IMAGE_TAG ]]; then echo "ERROR variable IMAGE_TAG is not defined "; exit 1; fi 
        - docker rmi -f $CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG || echo "image $CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG does not exist"
        - echo $CI_BUILD_TOKEN | docker login -u gitlab-ci-token --password-stdin gitlab-registry.cern.ch
        - docker build --no-cache -t $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-${CI_COMMIT_SHA:0:8} -f $DOCKERFILE $CONTEXT
        - docker tag $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-${CI_COMMIT_SHA:0:8} $CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG
        - docker tag $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-${CI_COMMIT_SHA:0:8} $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-latest
        - docker push $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-${CI_COMMIT_SHA:0:8}
        - docker push $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-latest
        - docker push $CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG
    after_script:
        - docker rmi $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-${CI_COMMIT_SHA:0:8}
        - docker rmi $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-latest
        - docker rmi $CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG

############################################################
#####              TEST IMAGE                          #####
############################################################

.template_test_qa:
    # This job installs hep-spec  
    # then runs a configurable set of benchmarks 
    stage: test-image
    image: 
      # Using ${CI_COMMIT_SHORT_SHA} to reference the image tag, instead of ${CI_COMMIT_TAG:-${CI_COMMIT_BRANCH}}
      # because at container start the condition ${CI_COMMIT_TAG:-${CI_COMMIT_BRANCH}} is not evaluted
      name: $CI_REGISTRY_IMAGE/hepspec-cc7-multiarch:ci-${CI_COMMIT_SHORT_SHA}
      entrypoint: [""]
    tags: 
      -  hepspec-docker-runner
    needs: ["build_multiarch_manifest"]
    script: 
      - export BMK_VOLUME=/scratch/jobs/${CI_JOB_NAME}_${CI_JOB_ID}
      - export SPEC_DIR=/scratch/HEPSPEC/CI_${CI_JOB_NAME}_${CI_JOB_ID}
      - BMK_OPTION=${BMK_OPTION:-'default'}
      - source $CI_PROJECT_DIR/test/config_ci_tests.sh
      - echo ${SECRET_URL} | /hep-spec/scripts/hep-spec.sh -w $BMK_RUNDIR -b $BMK -m $BMK_OPTION -n 1 -i 1 -p ${SPEC_DIR} -s $BMK_SET -u -
    after_script:
      - export BMK_VOLUME=/scratch/jobs/${CI_JOB_NAME}_${CI_JOB_ID}
      - export SPEC_DIR=/scratch/HEPSPEC/CI_${CI_JOB_NAME}_${CI_JOB_ID}
      - source $CI_PROJECT_DIR/test/config_ci_tests.sh
      - tar -czf hepspec-test-$CI_JOB_ID.tgz ${BMK_RUNDIR}
      - du -csh  ${BMK_RUNDIR} ${SPEC_DIR}
      - rm -rf ${BMK_VOLUME} ${SPEC_DIR}
      - du -csh  ${BMK_RUNDIR} ${SPEC_DIR} || echo "The directories have been removed"
    only:
      variables:
        - $CI_COMMIT_BRANCH =~ /^qa-.*$/
        - $CI_COMMIT_BRANCH =~ /^qa$/
        - $CI_COMMIT_TAG =~ /^v.*$/
    artifacts:
      paths:
        - hepspec-test-$CI_JOB_ID.tgz
      expire_in: 1 week
      when: always

############################################################
#####              TEST QA SINGULARITY                 #####
############################################################
.template_test_singularity_qa_image: 
    # This job tests the hep-spec with singularity
    # running a configurable set of benchmarks
    stage: test-singularity-image
    needs: ["build_multiarch_manifest"]
    image: gitlab-registry.cern.ch/hep-benchmarks/hep-benchmark-suite/hep-benchmark-suite-cc7:prior
    script:
      - export BMK_VOLUME=/scratch/jobs/${CI_JOB_NAME}_${CI_JOB_ID}
      - export SPEC_DIR=/scratch/HEPSPEC/CI_${CI_JOB_NAME}_${CI_JOB_ID}
      - export SINGULARITY_CACHEDIR=/scratch/jobs/singularity_cache
      - export SINGULARITY_IMAGE=/scratch/jobs/hepspec-cc7.sif
      - $CI_PROJECT_DIR/test/test_singularity.sh
    after_script:
      - export BMK_VOLUME=/scratch/jobs/${CI_JOB_NAME}_${CI_JOB_ID}
      - export SPEC_DIR=/scratch/HEPSPEC/CI_${CI_JOB_NAME}_${CI_JOB_ID}
      - source $CI_PROJECT_DIR/test/config_ci_tests.sh
      - du -csh ${BMK_RUNDIR} ${SPEC_DIR}
      - tar -czf hepspec-test-$CI_JOB_ID.tgz ${BMK_RUNDIR}
      - rm -rf ${BMK_VOLUME} ${SPEC_DIR}
      - du -csh ${BMK_RUNDIR} ${SPEC_DIR} || echo "The directories have been removed"
    only:
     variables:
       - $CI_COMMIT_BRANCH =~ /^qa-.*$/
       - $CI_COMMIT_BRANCH =~ /^qa$/
    artifacts:
        paths:
          - hepspec-test-$CI_JOB_ID.tgz
        expire_in: 1 week
        when: always