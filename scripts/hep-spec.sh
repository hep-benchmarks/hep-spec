#!/usr/bin/env bash

###############################################################################
# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level directory
# of this distribution. For licensing information, see the COPYING file at
# the top-level directory of this distribution.
###############################################################################

function usage(){
    echo -e "Usage: $HEPSPEC_SCRIPT [OPTIONS]

    OPTIONS:
    -h
    \t Display this help and exit
    -d
    \t Debug verbosity
    -b <string> 
    \t The benchmark to run.
    \t Default: hs06. Options: hs06, spec2017. 
    -n <int>
    \t Number of concurrent processes (usually cores) to run. 
    \t Default is number of processor in /proc/cpuinfo
    -p <string>
    \t MANDATORY: Path where the HS06 (or SPEC-CPU2017) installation is expected. 
    -u <string>
    \t url where the HS06 (or SPEC-CPU2017) tarball is expected to be downloaded.
    \t If specify '-' is specified, the url will be read from standard input.
    \t The tarball is then unpacked into path specified by -p 
    -s <string> 
    \t the benchmark set as defined in the HS06 (SPEC-CPU2017) documentation. 
    \t Default all_cpp for HS06 is used and pure_rate_cpp for SPEC-CPU2017. 
    \t Example -s 453.povray or -s 511.povray_r  
    -m <string> 
    \t the gcc compiler flag -m used to define the CPU model.
    \t Default is 'default' and implies that the best known choice will be applied.
    \t Example -m 64 or -m little-endian
    -i <int>
    \t the number of iterations for each benchmark in the HS06 (SPEC-CPU2017) suite. 
    \t Default is 3.
    -w <string>
    \t working dir. 
    \t Default: $HEPSPEC_WORKDIR
    -c <configfile>
    \t Use customized config file. This overrides the defaults agreeds for HS06/SPEC2017 
    \t and should be used with care, because can invalidate the results.
    "
}

function run_hs06() {
    # Install HS06 (if SPEC_URL is provided) and run HS06

    prepare_spec "HS06" "bin/runspec" "$HEPSPEC_PATH" "$HEPSPEC_URL"
    if [[ $? -ne 0 ]]; then
        myecho "[run_hs06] ERROR: prepare_spec failed. Won't run HS06"
        HEPSPEC_EXIT_STATUS=1
        exit ${HEPSPEC_EXIT_STATUS}
    fi

    HS06_INSTALLATION_PATH=$(pwd)

    chmod u+w -R ${HS06_INSTALLATION_PATH}/config
    cp ${HEPSPEC_SOURCEDIR}/spec2k6/*.cfg ${HS06_INSTALLATION_PATH}/config
    if [[ $? -ne 0 ]];
    then
        myecho "[run_hs06] Failing to copy config file ${HEPSPEC_SOURCEDIR}/spec2k6/linux*-gcc_cern.cfg to ${HS06_INSTALLATION_PATH}/config" 
        HEPSPEC_EXIT_STATUS=1
        exit ${HEPSPEC_EXIT_STATUS}
    fi

    mkdir -p ${HEPSPEC_WORKDIR}/HS06
    . ${HEPSPEC_SOURCEDIR}/spec2k6/runhs06.sh

    # Change to support multiple architectures
    RUNNING_ARCH=`uname -m`

    if [[ ${HEPSPEC_CONFIG} == "" ]];
    then
        HEPSPEC_CONFIG=linux_gcc_cern.cfg
        myecho "[run_hs06] using default configuration ${HEPSPEC_CONFIG}"
        if [[ ${HEPSPEC_MACHINE_OPTION} == "default" ]]; then
            # in case model not specified adopt the 64 bits
            HEPSPEC_MACHINE_OPTION="64"
            myecho "[run_hs06] Setting HEPSPEC_MACHINE_OPTION to ${HEPSPEC_MACHINE_OPTION}"
        fi
        if [[ (${RUNNING_ARCH}  =~ ^aarch64*$) && ($HEPSPEC_MACHINE_OPTION != "64" ) ]]; then
            myecho "[run_hs06] ERROR: supported machine option is -m 64. Exit"
            return 1
        elif [[ (${RUNNING_ARCH}  =~ ^x86.*$) && !($HEPSPEC_MACHINE_OPTION  =~ ^(32|64)$) ]]; then
            myecho "[run_hs06] ERROR: supported machine option for x86 is -m 32 or -m 64. Exit"
            return 1
        elif [[ !(${RUNNING_ARCH}  =~ ^(aarch64*|x86.*)$) ]]; then
            myecho "[run_hs06] Failed to identify supported Architecture using uname -m. Result is `uname -m`" 
            return 1
        fi
    else
        myecho "[run_hs06] ATTENTION: using custom configuration ${HEPSPEC_CONFIG} at your own risk"
    fi

    myecho "[run_hs06] running runhs06 -c ${HEPSPEC_CONFIG} -m ${HEPSPEC_MACHINE_OPTION} -f ${HEPSPEC_WORKDIR}/HS06 -s ${HS06_INSTALLATION_PATH} -n ${HEPSPEC_NUMPROC} -i ${HEPSPEC_ITERATIONS} -b ${HEPSPEC_SET}"
    runhs06 -c "${HEPSPEC_CONFIG}" -m "${HEPSPEC_MACHINE_OPTION}" -f "${HEPSPEC_WORKDIR}/HS06" -s ${HS06_INSTALLATION_PATH} -n ${HEPSPEC_NUMPROC} -i ${HEPSPEC_ITERATIONS} -b ${HEPSPEC_SET}
    [[ $? -ne 0 ]] && myecho "[run_hs06] Failed runhs06. Exit" && return 1
    return 0
}

function run_spec2017() {
    # prepare environment and run script for SPEC2017
    prepare_spec "SPEC2017" "bin/runcpu" "$HEPSPEC_PATH" "$HEPSPEC_URL"

    if [[ $? -ne 0 ]]; then
        myecho "[run_spec2017] ERROR: prepare_spec failed. Won't run SPEC2017"
        HEPSPEC_EXIT_STATUS=1
        exit ${HEPSPEC_EXIT_STATUS}
    fi
    SPEC2017_INSTALLATION_PATH=$(pwd)

    chmod u+w -R ${SPEC2017_INSTALLATION_PATH}/config ${SPEC2017_INSTALLATION_PATH}/benchspec/CPU/
    cp ${HEPSPEC_SOURCEDIR}/spec2017/*.cfg ${SPEC2017_INSTALLATION_PATH}/config && cp ${HEPSPEC_SOURCEDIR}/spec2017/*.bset ${SPEC2017_INSTALLATION_PATH}/benchspec/CPU/
    [[ $? -ne 0 ]] && myecho "[run_spec2017] Failing to copy config file ${HEPSPEC_SOURCEDIR}/spec2017/\*.cfg or ${HEPSPEC_SOURCEDIR}/spec2017/\*.bset to ${SPEC2017_INSTALLATION_PATH}/config" && return 1

    mkdir -p "${HEPSPEC_WORKDIR}/SPEC2017"
    . "${HEPSPEC_SOURCEDIR}/spec2017/runspec2017.sh"

    RUNNING_ARCH=`uname -m`

    if [[ ${HEPSPEC_CONFIG} == "" ]];
    then
        if [[ ${RUNNING_ARCH}  =~ ^x86.*$ ]]; then
            # x86 arch
            if [[ "${HEPSPEC_MACHINE_OPTION}" == "default" ]]; then
                # in case model not specified, default to 64 bits
                HEPSPEC_MACHINE_OPTION="64"
                myecho "[run_spec2017] Setting HEPSPEC_MACHINE_OPTION to ${HEPSPEC_MACHINE_OPTION}"
            fi
            HEPSPEC_CONFIG="cern-gcc-linux-x86.cfg"
            myecho "[run_spec2017] using default configuration ${HEPSPEC_CONFIG}"
        elif [[ (${RUNNING_ARCH}  =~ ^arm.*$) || (${RUNNING_ARCH}  =~ ^aarch64*$) ]]; then
            # ARM arch
            if [[ "${HEPSPEC_MACHINE_OPTION}" == "default" ]]; then
                if [[ (${RUNNING_ARCH}  =~ ^arm.*$) ]];then
                    HEPSPEC_MACHINE_OPTION="abi=aapcs"
                elif [[ (${RUNNING_ARCH}  =~ ^aarch64*$) ]]; then
                    HEPSPEC_MACHINE_OPTION="little-endian"
                # Add here others when known
                fi
                myecho "[run_spec2017] Setting HEPSPEC_MACHINE_OPTION to ${HEPSPEC_MACHINE_OPTION}"
            fi
            HEPSPEC_CONFIG="cern-gcc-linux-aarch64.cfg"
            myecho "[run_spec2017] using default configuration ${HEPSPEC_CONFIG}"
        #elif [[ ${RUNNING_ARCH}  =~ ^ppc64le$ ]]; then
            # Prepare for PowerPC
            #    runspec2017 -f "${HEPSPEC_WORKDIR}/SPEC2017" -s "${SPEC2017_INSTALLATION_PATH}" -n "${HEPSPEC_NUMPROC}" -i "${HEPSPEC_ITERATIONS}" -b "${HEPSPEC_SET}" -m "${HEPSPEC_MACHINE_OPTION}" -c "cern-gcc-linux-ppc64le.cfg"
        else
            myecho "[run_spec2017] Failed to identify supported Architecture using uname -m. Result is `uname -m`" 
            return 1
        fi
    else
        myecho "[run_spec2017] ATTENTION: using custom configuration ${HEPSPEC_CONFIG} at your own risk"
    fi

    myecho "[run_spec2017] running runspec2017 -f ${HEPSPEC_WORKDIR}/SPEC2017 -s ${SPEC2017_INSTALLATION_PATH} -n ${HEPSPEC_NUMPROC} -i ${HEPSPEC_ITERATIONS} -b ${HEPSPEC_SET} -m ${HEPSPEC_MACHINE_OPTION} -c ${HEPSPEC_CONFIG}"
    runspec2017 -f "${HEPSPEC_WORKDIR}/SPEC2017" -s "${SPEC2017_INSTALLATION_PATH}" -n "${HEPSPEC_NUMPROC}" -i "${HEPSPEC_ITERATIONS}" -b "${HEPSPEC_SET}" -m "${HEPSPEC_MACHINE_OPTION}" -c "${HEPSPEC_CONFIG}"
    [[ $? -ne 0 ]] && myecho "[run_spec2017] Failed runspec2017. Exit" && return 1
    return 0
}

function extract_tarball() {
    INSTALL_PATH=$1
    TAR_URL=$2
    [[ ! -e ${INSTALL_PATH}/tmp_download ]] && mkdir -p ${INSTALL_PATH}/tmp_download
    
    # Check if the TAR_URL is a local file
    if [[ $TAR_URL =~ ^file\:.* ]]; then
        myecho "[extract_tarball] tarball file is local, copying it to ${INSTALL_PATH}/tmp_download/tar_file"
        cp -v ${TAR_URL#file:} ${INSTALL_PATH}/tmp_download/tar_file
        if [[ $? -ne 0 ]]; then
            myecho "ERROR accessing package. Verify that the local file passed with option -u is correct: file:/absolute_path_to_tar_file"
            rm -rf ${URL_PATH}/tmp_download
            HEPSPEC_EXIT_STATUS=1
            exit ${HEPSPEC_EXIT_STATUS}
        fi
    else 
        echo ${TAR_URL} |  wget -q -O ${INSTALL_PATH}/tmp_download/tar_file -i -
        if [[ $? -ne 0 ]]; then
            myecho "ERROR downloading package. Verify that the url passed with option -u is correct"
            rm -rf ${URL_PATH}/tmp_download
            HEPSPEC_EXIT_STATUS=1
            exit ${HEPSPEC_EXIT_STATUS}
        fi
    fi
    myecho "[extract_tarball] INSTALL_PATH: $INSTALL_PATH"
    cd ${INSTALL_PATH}
    myecho "[extract_tarball] untar tarball"
    tar --no-overwrite-dir -xak -f ${INSTALL_PATH}/tmp_download/tar_file 2>/dev/null || myecho "tar executing with errors" # (BMK-365 avoid overwrite of existing folders)
    chmod u+w -R ${INSTALL_PATH}
    myecho "[extract_tarball] untar tarball done"
    rm -rf ${INSTALL_PATH}/tmp_download                                  # in order to reduce space occupancy
}

function prepare_spec() {
    #function to check the SPEC configuration (download path, running path)
    #and in case untar the application

    SPECNAME=$1
    SPECEXE=$2
    SPECPATH=$3
    SPECURL=$4

    myecho "[prepare_spec] SPECNAME $SPECNAME"
    myecho "[prepare_spec] SPECEXE  $SPECEXE"
    myecho "[prepare_spec] SPECPATH $SPECPATH"
    myecho "[prepare_spec] SPECURL  ***"

    [[ ! -e ${HEPSPEC_WORKDIR}/${SPECNAME} ]] && mkdir -p ${HEPSPEC_WORKDIR}/${SPECNAME}

    #This path is mandatory. The SPEC2017 installation is expected to be here or to be downlaoded here
    if [[ -z ${SPECPATH} ]]; then
        myecho "[prepare_spec] ERROR: Unable to find directory for ${SPECNAME}. Please define it using -p your_path_to_it. Exit from run_${SPECNAME,,} without running."
        HEPSPEC_EXIT_STATUS=1
        exit ${HEPSPEC_EXIT_STATUS}
    fi

    # if user requests to download from a url then
    # download the file in $SPECUR and untar it, to be then in $SPECPATH
    # NB: if the dir $SPECPATH already exists, the untar will add files
    if [[ ! -z ${SPECURL} ]]; then
        extract_tarball ${SPECPATH} ${SPECURL}
        if [[ $? -ne 0 ]]; then
            myecho "[prepare_spec] Exit from ${SPECNAME}"
            HEPSPEC_EXIT_STATUS=1
            exit ${HEPSPEC_EXIT_STATUS}
        fi
    fi

    # At this point the dir ${SPECPATH} must exist
    if [[ ! -d ${SPECPATH} ]]; then
        myecho "[prepare_spec] ERROR: Unable to find directory for ${SPECNAME}. \
    Please install ${SPECNAME} in the directory specified by  -p=your_path_to_it, \
    or provide a url to download ${SPECNAME} using the argument -u. Exit from run_${SPECNAME,,} without running"
        HEPSPEC_EXIT_STATUS=1
        exit ${HEPSPEC_EXIT_STATUS}
    fi

    #find the SPEC dir, it could be in a subdir
    cd ${SPECPATH}/
    myecho "[prepare_spec] in SPECPATH " $(pwd)
    myecho "[prepare_spec] looking for ${SPECEXE}"
    CHECKPATH=$(find . -path "*${SPECEXE}")
    myecho "[prepare_spec] variable CHECKPATH is $CHECKPATH"
    if [[ -z ${CHECKPATH} ]]; then
        myecho "[prepare_spec] ERROR: unable to find ${SPECEXE} in the path ${SPECPATH}. Exit from run_${SPECNAME,,} without running"
        HEPSPEC_EXIT_STATUS=1
        exit ${HEPSPEC_EXIT_STATUS}
    fi
    SPECDIR=$(readlink -f $(dirname "$CHECKPATH")/..)
    myecho "[prepare_spec] moving to dir $SPECDIR"
    cd $SPECDIR #bin/runspec
}


function advertise_variables() {
    myecho "Variable values:"
    for var in HEPSPEC_SOURCEDIR HEPSPEC_BMK HEPSPEC_NUMPROC HEPSPEC_PATH HEPSPEC_SET HEPSPEC_MACHINE_OPTION HEPSPEC_ITERATIONS HEPSPEC_WORKDIR HEPSPEC_DEBUG; 
    do
        echo -e "\t$var=${!var}"
    done
}

function myecho(){
    echo "$(date +%Y-%m-%dT%T) [${HEPSPEC_SCRIPT}] $@"
}
# Check that mandatory variables have been defined (default values)
function check_mandatory_variables(){
  # Variables NCOPIES, NTHREADS, NEVENTS_THREAD have default values specific to each benchmark
    for var in HEPSPEC_PATH HEPSPEC_SET; 
    do
    if [ "${!var}" == "" ]; then
      myecho "[${HEPSPEC_SCRIPT}] ERROR! A default value of $var must be set"
      usage
      HEPSPEC_EXIT_STATUS=1
      exit ${HEPSPEC_EXIT_STATUS};
    fi
  done
}

set -e
HEPSPEC_EXIT_STATUS=1

echo "
##############################
          CERN HEPSPEC     
$(date)
##############################
"

HEPSPEC_SOURCEDIR=$(readlink -f $(dirname $0))
HEPSPEC_SCRIPT=$(readlink -f $0)

HEPSPEC_BMK="hs06"
HEPSPEC_NUMPROC=$(grep -c "^processor" /proc/cpuinfo)
HEPSPEC_PATH=""
HEPSPEC_URL=""
HEPSPEC_SET=""
HEPSPEC_MACHINE_OPTION="default"
HEPSPEC_ITERATIONS=3
HEPSPEC_WORKDIR="/tmp/hepspec_workdir"
HEPSPEC_DEBUG=0
HEPSPEC_CONFIG=""

while getopts "hdb:n:p:u:s:m:i:w:c:" flag; do
    case $flag in
    b) HEPSPEC_BMK=$OPTARG ;;
    n) HEPSPEC_NUMPROC=$OPTARG ;;
    p) HEPSPEC_PATH=$OPTARG ;;
    u) HEPSPEC_URL=$OPTARG 
        if [ "${HEPSPEC_URL}" = "-" ]; then
            HEPSPEC_URL=`cat`
        fi
    ;;
    s) HEPSPEC_SET=$OPTARG ;;
    m) HEPSPEC_MACHINE_OPTION=$OPTARG ;;
    i) HEPSPEC_ITERATIONS=$OPTARG ;;
    w) HEPSPEC_WORKDIR=$(readlink -m ${OPTARG}) ;;
    d) HEPSPEC_DEBUG=1 ;;
    c) HEPSPEC_CONFIG=$OPTARG;;
    h)
        usage
        HEPSPEC_EXIT_STATUS=0
        exit ${HEPSPEC_EXIT_STATUS}
        ;;
    *)
        usage
        HEPSPEC_EXIT_STATUS=1
        exit ${HEPSPEC_EXIT_STATUS}
        ;;
    esac
done

[ "$HEPSPEC_DEBUG" == "1" ] && myecho "Setting DEBUG " && set -x

# set default HEPSPEC_SET for spec2017
if [[ ("$HEPSPEC_BMK" == "spec2017") && ("$HEPSPEC_SET" == "") ]]; then
    HEPSPEC_SET="pure_rate_cpp"
fi

# set default HEPSPEC_SET for hs06
if [[ ("$HEPSPEC_BMK" == "hs06") && ("$HEPSPEC_SET" == "") ]]; then
    HEPSPEC_SET="all_cpp"
fi


check_mandatory_variables
advertise_variables

# Create working dir
if [ ! -e $HEPSPEC_WORKDIR ];
then
    myecho "working dir $HEPSPEC_WORKDIR does not exist. Creating it"
    mkdir -p $HEPSPEC_WORKDIR
    chmod 777 $HEPSPEC_WORKDIR
else
    myecho "working dir $HEPSPEC_WORKDIR already exists."
fi

if [[ ${HEPSPEC_CONFIG} != "" ]]; then
    myecho "ATTENTION: using a custom config file ${HEPSPEC_CONFIG}. Make sure it is available in the ${HEPSPEC_PATH}/<SPEC_dir>/config folder"
fi

if [[ ${HEPSPEC_BMK} =~ ^(hs06)$ ]]; then
    run_hs06 "${HEPSPEC_BMK}" | tee $HEPSPEC_WORKDIR/hep-spec_hs06.log 
    HEPSPEC_EXIT_STATUS=${PIPESTATUS[0]}
elif [[ ${HEPSPEC_BMK} =~ ^spec2017$ ]]; then
    run_spec2017 | tee $HEPSPEC_WORKDIR/hep-spec_spec2017.log
    HEPSPEC_EXIT_STATUS=${PIPESTATUS[0]}
else
    myecho "WARNING: benchmark ${HEPSPEC_BMK} is not implemented"
    usage
    HEPSPEC_EXIT_STATUS=1
    exit ${HEPSPEC_EXIT_STATUS}
fi

myecho "Finishing. Exit status ${HEPSPEC_EXIT_STATUS}"
exit ${HEPSPEC_EXIT_STATUS}
