#!/bin/bash

###############################################################################
# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level directory
# of this distribution. For licensing information, see the COPYING file at
# the top-level directory of this distribution.
###############################################################################

function usage () {
    echo "Usage: $0 [OPTIONS]"
    echo
    echo "Where OPTIONS can be:"
    echo " -h"
    echo "    Display this help and exit"
    echo " -f /afs/cern.ch/..."
    echo "    Directory where the results will be dumped. By default, it is"
    echo "    $RESULTDIR."
    echo " -m machine option"
    echo "    gcc machine option as from https://man7.org/linux/man-pages/man1/g++.1.html."
    echo "    if not defined will be defined in the cfg."
    echo "    Examples -m 64"
    echo " -r"
    echo "    Run a rate metric, instead of the default speed metric."
    echo " -b benchmark"
    echo "    Benchmark to run: int, fp or all_cpp. The default is $BENCHMARK."
    echo " -s specdir"
    echo "    Directory where the benchmark is installed. The default is $SPECDIR."
    echo " -c configfile"
    echo "    Use customized config file. This overrides the -a option."
    echo " -n number_of_copies"
    echo "    Number of benchmark copies to be started at the same time in rate or"
    echo "    parallel runs. The default is $COUNT."
    echo " -p"
    echo "    Reuse existing SPEC installation (in ${SPECDIR}/) and don't clean up."
    echo " -i"
    echo "    number of iterations (default=3)"
    echo
}

function fail () {
    echo $1 >&2
    exit 1
}

function echo_msg() {
    echo -e "$(date +%Y-%m-%dT%T) [$0] $@"
}

function calculate_results() {
    echo_msg "[calculate_results]"

    SPECDIR=$1
    # Calculate the result
    # First, lets get a list of the runs to look for (001, 002, etc.)
    RUNS=""
    for n in $SPECDIR/result/CPU2006.*.log;
    do
	RUNS="$RUNS `echo $n | sed 's/^.*CPU2006\.\(\w\+\)\.log/\1/'`"
    done

    SUM=0
    TOTRUNS=0
    LISTBMKS=""
    mincount=1000
    maxcount=0
    for n in $RUNS;
    do
	partial=0
	count=0
	# This scary-looking sed expression looks in the results files of a single run
	# (both CINT and CFP files) for the stuff between a line containing all =====,
	# and " Est. SPEC". This is the final results table and lists all the partial results.
	# Within that section, look for lines that look like:
	#   410.bwaves      13590       2690       5.05 *
	# and grab the last number, 5.05
	for b in `sed -n -e '/^=\+$/,/^ Est. SPEC/!d; s/[0-9]\{3\}\.\w\+\s\+[0-9]\+\s\+[0-9]\+\s\+\([0-9.]\+\)\s\+\*/\1/p' $SPECDIR/result/*.$n.*txt 2>/dev/null`;
	do
            partial="$partial + l($b)"
            count=$(($count + 1))
	done
	if [[ $partial != 0 ]]; # "if the above sed read something..."
	then
            # Calculate the geometric average of all the benchmark results for that run (ie. core)
            # The geometric average of three numbers is: (x * y * z)**1/3
            # or, in order to process this with bc: exp( ( ln(x) + ln(y) + ln(z) ) / 3 )
            SUM="$SUM + `echo "scale=8; e(($partial) / $count)" | bc -l`"

	    LISTBMKS="$LISTBMKS `sed -n -e '/^=\+$/,/^ Est. SPEC/!d; s/\([0-9]\{3\}\.\w\+\)\s\+[0-9]\+\s\+[0-9]\+\s\+\([0-9.]\+\)\s\+\*/\1/p' $SPECDIR/result/*.$n.*txt 2>/dev/null`"

	    [[ $count -lt $mincount ]] && mincount=$count
	    [[ $count -gt $maxcount ]] && maxcount=$count
	    TOTRUNS=$(($TOTRUNS+1))
	fi
    done

    [[ $mincount -ne $maxcount ]] && echo "WARNING: potential error. The number of benchmark results is not equal in all runs" && return 1

    # Add up all the geometric averages and round to the second decimal
    AVERAGE=0
    [[ $TOTRUNS -gt 0 ]] && AVERAGE=`echo "scale=3; ($SUM)/$TOTRUNS" | bc | awk '{printf "%.3f", $0 }'`
    SUM=`echo "scale=3; ($SUM)/1" | bc | awk '{printf "%.3f", $0 }'`
    echo "Final result: $SUM  over $TOTRUNS runs. Average $AVERAGE"

    #The file result/CPU2006.001.log is the first generated doing build
    BSET=`grep "action=build" $SPECDIR/result/CPU2006.001.log | awk -F 'action=build' '{print $2}' | sed -e 's@ @@g' | uniq`
    LINK=`grep 'LINK' $SPECDIR/result/CPU2006.001.log | cut -d":" -f2 | uniq -c | tr "\n" ";" | sed -e 's@   @ @g'`
    RUNCPU_ARGS=`grep 'runspec:' $SPECDIR/result/CPU2006.*.log | cut -d":" -f2- | uniq  -c | tr "\n" ";" | sed -e 's@\s\{2,10\}@@g'`
    #Now build the JSON output that will be used for the suite
    JSON="{\"hs06\":{
        \"start\":\"$START\", 
        \"end\":\"$END\", 
        \"copies\":$TOTRUNS, 
        \"machine_option\":\"$MACHINE_OPTION\",
        \"runcpu_args\":\"$RUNCPU_ARGS\", 
        \"bset\":\"$BSET\", 
        \"LINK\":\"$LINK\", 
        \"hash\":\"${CONFIG_HASH:-invalid}\",
        \"score\":$SUM, 
        \"avg_core_score\" : $AVERAGE, 
        \"num_bmks\":$count ,
        \"bmks\":{"
    for bmk in `echo $LISTBMKS | tr " " "\n" | sort | uniq`;
    do
	reslist=""
	for res in `sed -n -e "/^=\+$/,/^ Est. SPEC/!d; s/$bmk\s\+[0-9]\+\s\+[0-9]\+\s\+\([0-9.]\+\)\s\+\*/\1/p" $SPECDIR/result/*.[0-9]*.*txt 2>/dev/null`;
	do
	    reslist="$reslist $res," 
	done
	JSON="$JSON \"$bmk\":[ ${reslist%,}],"
    done
    JSON="${JSON%,} }}}"

}

function summary_report() {
    echo_msg "[summary_report]"

    # Put together the system description file
    echo -n "SPEC${BENCHMARK}2006 " >> $NAME/system.txt
    if [ $RATE ]; then echo -n "rate " >> $NAME/system.txt; fi
 	echo "with custom config file $CONFIG." >> $NAME/system.txt;
    echo "Description:" $DESCRIPTION >> $NAME/system.txt
    echo "Result:" $SUM >> $NAME/system.txt
    for n in $RUNS;
    do
	echo "Values RUN $n " `sed -n -e '/^=\+$/,/^ Est. SPEC/!d; s/[0-9]\{3\}\.\w\+\s\+[0-9]\+\s\+[0-9]\+\s\+\([0-9.]\+\)\s\+\*/\1/p' $NAME/*.$n.*txt 2>/dev/null` >> $NAME/system.txt
	
    done

    echo "Start time:" $START >> $NAME/system.txt
    echo "End time:  " $END >> $NAME/system.txt
    echo >> $NAME/system.txt
    echo "Kernel: `uname -a`" >> $NAME/system.txt
    echo "Processors: `grep -c vendor /proc/cpuinfo` `cat /proc/cpuinfo | grep -m 1 "model name" | cut -d":" -f 2`" >> $NAME/system.txt
    echo "Memory: `grep MemTotal /proc/meminfo | grep -oe '[[:digit:]]\+ kB'`" >> $NAME/system.txt
    echo "GCC: `$USED_CC  --version | head -n1`" >> $NAME/system.txt
    echo "C++: `$USED_CXX --version | head -n1`" >> $NAME/system.txt
    echo "FC:  `$USED_FC  --version | head -n1`" >> $NAME/system.txt
    echo "SPEC2006 version: `cat $SPECDIR/version.txt`" >> $NAME/system.txt
    echo >> $NAME/system.txt
    file $SPECDIR/benchspec/CPU2006/*/exe/*_cern | cut -d"/" -f 4,6- >> $NAME/system.txt
    echo >> $NAME/system.txt
    cat /proc/cpuinfo >> $NAME/system.txt
    echo >> $NAME/system.txt
    (dmidecode 2>/dev/null || ([ -e dmidecode-output ] && cat dmidecode-output) || echo "No dmidecode output, please run as root, or run this command first: dmidecode > $PWD/dmidecode-output") >> $NAME/system.txt
    echo >> $NAME/system.txt
    (/sbin/lspci || echo "No lspci output, please run as root") >> $NAME/system.txt
}

function runhs06() {
    echo_msg "[runhs06]"
    SPECDIR="/HS06"
    RESULTDIR="."
    BENCHMARK="all_cpp"
    COUNT=`grep -c "^processor" /proc/cpuinfo`;
    MACHINE_OPTION='64'
    CONFIG='linux_gcc_cern.cfg'
    ITERATIONS=3

    local OPTIND
    while getopts "hf:m:rb:s:c:n:i:" flag;
    do
    case $flag in
            f ) RESULTDIR=$OPTARG;;
            m ) MACHINE_OPTION=$OPTARG;;
            r ) RATE=1;;
            b ) BENCHMARK=$OPTARG;;
            s ) SPECDIR=$OPTARG;;
            c ) CONFIG=$OPTARG;;
            n ) COUNT=$OPTARG;;
            i ) ITERATIONS=$OPTARG;;
            h ) usage
        return 1;;
            * ) usage
        return 1;;
    esac
    done

	echo_msg "[runhs06] INFO: HS06 CONFIG file is $CONFIG" 

    if [[ ! -e $SPECDIR/config/$CONFIG ]]; then
        fail "[$0] [runhs06] Configuration file $CONFIG does not exist"
    fi

    if [[ !($MACHINE_OPTION =~ ^(32|64)$) ]]; then
        myecho "[run_hs06] ERROR: supported machine option is -m 32 or -m 64. Exit"
        return 1
    fi

    NAME="spec2k6-`hostname -s`-`date +%Y%m%d-%H%M%S`"
    START=`date`


    cd $SPECDIR || fail "[$0] [runhs06] Unable to find HS06 directory (should be $SPECDIR)"
    SPECDIR=`pwd` #get the absolute path

    CONFIG_FILE_HASH='invalid'
    if [[ -e config/${CONFIG} ]]; then
        CONFIG_FILE_HASH=`md5sum config/${CONFIG}`
    fi

    if [[ (! -e VERIFIED_INSTALLATION)]];
    then
        ./install.sh -f && touch VERIFIED_INSTALLATION || fail "[$0] [runhs06] HS06 install failed!" 
    else
    echo_msg "[runhs06] INFO: HS06 installation already verified. Skipping it. If you want to run it in any case remove the file "`pwd`"/VERIFIED_INSTALLATION"
    fi

    # remember some information from the config file
    USED_CC=`cat  config/${CONFIG}| grep CC| cut -d'=' -f2`
    USED_CXX=`cat config/${CONFIG}| grep CXX| cut -d'=' -f2`
    USED_FC=`cat  config/${CONFIG}| grep FC| cut -d'=' -f2`

    # Set up the environment, clean all previous binaries and results, 
    # and then compile the binaries
    echo_msg "[runhs06] start build"
    . shrc
    runspec --define machine_option:${MACHINE_OPTION} --config=${CONFIG} --action=scrub $BENCHMARK || fail "[$0] [runhs06] Error during HS06 cleanup!"
    rm -f result/*
    runspec --define machine_option:${MACHINE_OPTION} --config=${CONFIG} --action=build $BENCHMARK > out_build.out || fail "[$0] [runhs06] Error during HS06 compile!"


    # Now we're ready to go. 
    CONFIG_HASH="invalid"
    echo_msg "[runhs06] start benchmark runs"
    if [ $RATE ]; then
	    runspec --define machine_option:${MACHINE_OPTION} --config=${CONFIG} --nobuild --noreportable --iterations=$ITERATIONS --rate $COUNT $BENCHMARK > /dev/null || fail "[$0] [runhs06] Error during HS06 rate execution!" &
        CONFIG_HASH=`echo "--define machine_option:${MACHINE_OPTION} --iterations=$ITERATIONS $BENCHMARK ${CONFIG_FILE_HASH} --rate " | md5sum | cut -f1 -d" "`
    else
        CONFIG_HASH=`echo "--define machine_option:${MACHINE_OPTION} --iterations=$ITERATIONS $BENCHMARK ${CONFIG_FILE_HASH}" | md5sum | cut -f1 -d" "`
        for i in `seq $COUNT`;
        do
                runspec --define machine_option:${MACHINE_OPTION} --config=${CONFIG} --nobuild --noreportable --iterations=$ITERATIONS $BENCHMARK > /dev/null || fail "[$0] [runhs06] Error during HS06 execution!" &
        done
    fi;

    wait

    echo_msg "[runhs06] Execution of multiple runcpu copies done. Time to collect results and send them off"
    END=`date`

    calculate_results $SPECDIR
    
    # Prepare "results package"
    echo $JSON > $RESULTDIR/hs06_result.json
    echo_msg "[runhs06] results stored in $RESULTDIR/hs06_result.json"
    echo_msg "[runhs06] results:\n$JSON"

    mkdir -p $RESULTDIR/$NAME
    cp $SPECDIR/result/*.{txt,log} $RESULTDIR/$NAME/ || fail "[$0] [runhs06] Unable to copy results"
    cp $SPECDIR/config/${CONFIG} $RESULTDIR/$NAME/ || fail "[$0] [runhs06] Unable to copy configuration file"

    #summary_report
    # Tar the whole lot
    tar czf "$NAME.tar.gz" "$RESULTDIR/$NAME/"
}


