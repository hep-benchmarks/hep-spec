This directory contains 

1) the utility script [runspec2017.sh](runspec2017.sh) that triggers the execution of `runcpu` and the data collection.

2) a dedicated benchmark set [pure_rate_cpp.bset](pure_rate_cpp.bset) that includes all the rate cpp benchmarks from SPEC CPU 2017

3) a configuration file [cern-gcc-linux-x86.cfg](cern-gcc-linux-x86.cfg) used to configure `runcpu` for linux `x86`

The diff between this file and the similar file included in the SPEC suite `Example-gcc-linux-x86.cfg` is

```
> sdiff -w 300 Example-gcc-linux-x86.cfg cern-gcc-linux-x86.cfg | grep "|"

%   define  gcc_dir        /SW/compilers/GCC/Linux/x86_64/gcc-6.3.0										     |	%   define  gcc_dir      /usr # /opt/rh/devtoolset-2/root/usr/  #/SW/compilers/GCC/Linux/x86_64/gcc-6.3.0
   OPTIMIZE       = -g -O3 -march=native -fno-unsafe-math-optimizations  -fno-tree-loop-vectorize						     |	   OPTIMIZE       = -g -O3 -fPIC -pthread #-march=native -fno-unsafe-math-optimizations  -fno-tree-loop-vectorize
```

4) a configuration file [cern-gcc-linux-aarch64.cfg](cern-gcc-linux-aarch64.cfg) used to configure `runcpu`  for linux `arm`

```
> diff -w cern-gcc-linux-x86.cfg cern-gcc-linux-aarch64.cfg | egrep -v  "(<|>) #"

55c48
< %   define model        -m64
---
> %   define model        -mabi=lp64
57c50
< %   define model        -m32
---
> %   define model        -mabi=ilp32
62a56
> 
72c66
< label                = %{label}-m%{bits}
---
> label                = %{label}-%{bits}
108c102
<    threads          = 4   # EDIT to change number of OpenMP threads (see above)
---
>    threads          = 8   # EDIT to change number of OpenMP threads (see above)
158c152
< %   define suffix IA32
---
> %   define suffix AARCH32
160c154
< %   define suffix X64
---
> %   define suffix AARCH64
178,179c172
<    CPORTABILITY    = -DSPEC_CASE_FLAG
<    FPORTABILITY    = -fconvert=big-endian
---
>    PORTABILITY   = -DSPEC_CASE_FLAG -fconvert=big-endian
210,212d202
214c204
<    OPTIMIZE       = -g -O3 -fPIC -pthread #-march=native -fno-unsafe-math-optimizations  -fno-tree-loop-vectorize
---
>    OPTIMIZE         = -O3
```
