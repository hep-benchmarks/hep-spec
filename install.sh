#!/usr/bin/env bash 

set -e

function install_x86(){
    # Instructions to install packages only for x86

    yum install -y epel-release

    install_common

    echo "needed to include in centos6/7 the 32 bit libc dev package"
    [[ ! $(yum list installed glibc-devel.i686 2 &>/dev/null) ]] && yum install -y glibc-devel.i686 libstdc++-devel.i686 
    yum list installed glibc-devel.i686 libstdc++-devel.i686 
}

function install_aarch(){
    # Instructions to install packages only for x86

    # The current base image from centos does not contain 
    # EPEL yum repo for ARM
    if [ ! -e /etc/yum.repos.d/epel.repo ]; then
        cat > /etc/yum.repos.d/epel.repo << EOF
[epel]
name="Epel rebuild for armhfp"
baseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/
enabled=1
gpgcheck=0
EOF
    fi

    ls /etc/yum.repos.d/

    install_common
}

function install_common(){
# Instructions to install common packages

    yum install -y \
        util-linux  wget git \
        bc tar freetype perl jq unzip bzip2 \
        yum-plugin-ovl \
        gcc-gfortran

    echo -e "Check if gcc-c++ is installed otherwise install it"
    [[ ! $(yum list installed gcc-c++ 2 &>/dev/null) ]] && yum install -y gcc-c++
    yum list installed gcc-c++
}

INSTALL_SCRIPT=$(readlink -f $0)
SOURCE_DIR=$(readlink -f $(dirname $0))
echo "SOURCE_DIR $SOURCE_DIR"

cd ${SOURCE_DIR}
ls -l 

INSTALL_DIR=${INSTALL_DIR:-$HOME/.local/hep-spec}
echo "INSTALL_DIR ${INSTALL_DIR}"
if [ ! -e ${INSTALL_DIR} ];
then
    mkdir -p ${INSTALL_DIR}
else
    [ -e ${INSTALL_DIR}/scripts ] && rm -rf ${INSTALL_DIR}/scripts
fi
cp -vr ${SOURCE_DIR}/scripts ${INSTALL_DIR}/.
chmod a+x -R ${INSTALL_DIR}

# Check O/S version
if [ ! -f /etc/redhat-release ]; then
    echo "ERROR! O/S is not a RedHat-based Linux system"
    echo "ERROR! This script is only supported on CC7"
    exit 1
elif egrep -q "^CentOS Linux release 7" /etc/redhat-release; then
    export OS="cc7"
    echo "INFO: Found OS $OS"
else
    echo "ERROR! This script is only supported on CC7"
    exit 1
fi

ARCH=`uname -p`
echo "Detected CPU architecture $ARCH"
if [[ ${ARCH} =~ (^x86_(32|64)$) ]]; then
    echo "Installing $ARCH specific libraries"
    install_x86
elif [[ ${ARCH} =~ (^aarch64$) ]]; then
    echo "Installing $ARCH specific libraries"
    install_aarch
else
    echo "ERROR! This script is only supported on x86 and aarch architectures"
    exit 1
fi