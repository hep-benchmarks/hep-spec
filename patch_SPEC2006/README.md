The set of instructions in this folder are used to build the SPEC 2006 toolkit for ARM.

Given the age of SPEC CPU 2006, most of the perl libraries used in the SPEC 2006 toolkit
do not build anymore and have to be fixed for bugs.

The script patch_spec06_in_2021.sh will fix these issues, and build the toolkit as documented in 
https://www.spec.org/cpu2006/Docs/tools-build.html#how


Given a *writable* SPEC CPU 2006 area (SPEC_path), run the script as follow
```
git clone https://gitlab.cern.ch/hep-benchmarks/hep-spec.git
./hep-spec/patch_SPEC2006/patch_spec06_in_2021.sh $SPEC_path
```

NB: the script has 2 env variables to fix the toolkit folder name. The current values are
```
myOS=linux-centos7
myHW=aarch64
```
