#!/bin/bash -xe

#### Variables to define the name of the toolkit build
#### to be included in tools/bin
myOS=linux-centos7
myHW=aarch64
export MYTOOLS=$myOS-$myHW
###################################################

[[ -z $1 ]] && echo "Please specify SPEC dir path. Exit" && exit 1

THIS_SCRIPT=$(readlink -f $0)
SCRIPT_DIR=$(readlink -f $(dirname $0))
echo "SCRIPT_DIR $SCRIPT_DIR"

SPEC_DIR=$(readlink -f $1)
echo "SPEC_DIR $SPEC_DIR"

cd ${SPEC_DIR}
ls -l 

echo -e "-------------------------------------------------
the config.guess file must be replaced with new version available at 
http://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=config.guess;hb=HEAD
"
GUESS=${SCRIPT_DIR}/config.guess
for afile in `grep "has failed to recognize" -ir -l tools/*  `; do echo "replacing $afile with $GUESS" ; \cp  $GUESS $afile ;done

echo -e "-------------------------------------------------
Have to comment function gets that does not exist anymore
"
grep "_GL_WARN_ON_USE (gets," ${SPEC_DIR}/tools/src/* -ir 
for afile in `grep "_GL_WARN_ON_USE (gets," ${SPEC_DIR}/tools/src/* -ir -l`;do echo $afile; sed -i -e 's@_GL_WARN_ON_USE (gets,@//_GL_WARN_ON_USE (gets,@g' $afile; done
grep "_GL_WARN_ON_USE (gets," ${SPEC_DIR}/tools/src/* -ir 


echo -e "-------------------------------------------------
# Problem with perl-5-12.3 in ./tools/src/perl-5.12.3
# https://github.com/Perl/perl5/issues/17410
# Module: Time::Local
# Description
# Older perl versions - e.g. 5.26.2 - no longer pass tests due to the 50-year bug in Time::Local, which was fixed in 1.26:
# https://metacpan.org/changes/distribution/Time-Local#L22

Replacing ${SPEC_DIR}/tools/src/perl-5.12.3/ext/Time-Local/t/Local.t with version ${SCRIPT_DIR}/Local.t
"
diff -u ${SPEC_DIR}/tools/src/perl-5.12.3/ext/Time-Local/t/Local.t  ${SCRIPT_DIR}/Local.t

\cp ${SCRIPT_DIR}/Local.t ${SPEC_DIR}/tools/src/perl-5.12.3/ext/Time-Local/t/Local.t

echo "
Bug https://rt.cpan.org/Public/Bug/Display.html?id=124509
Replacing ${SPEC_DIR}/./tools/src/TimeDate-1.20/t/getdate.t with ${SCRIPT_DIR}/getdate.t
"

diff ${SPEC_DIR}/./tools/src/TimeDate-1.20/t/getdate.t  ${SCRIPT_DIR}/getdate.t
\cp ${SCRIPT_DIR}/getdate.t ${SPEC_DIR}/./tools/src/TimeDate-1.20/t/getdate.t

echo "
It's time to build, using buildtools
This will take a while. Build log @ ${SPEC_DIR}/${MYTOOLS}.buildlog.txt
"

export LD_LIBRARY_PATH=${SPEC_DIR}/tools/src/perl-5.12.3:$LD_LIBRARY_PATH

# from https://www.spec.org/cpu2006/Docs/tools-build.html#how
export FORCE_UNSAFE_CONFIGURE=1 
${SPEC_DIR}/tools/src/buildtools > ${SPEC_DIR}/$MYTOOLS.buildlog.txt 2>&1 


echo "
It's time to package the toolkit 
with name $MYTOOLS
"

cd ${SPEC_DIR}
. ./shrc

mkdir tools/bin/$MYTOOLS
echo \
 "For 64-bit aarch64 Linux systems running
                              CentOS Linux 7"\
  > tools/bin/$MYTOOLS/description

runspec -V > $MYTOOLS.runspec-V.txt 2>&1
runspec --test > $MYTOOLS.runspec--test.txt 2>&1

packagetools $MYTOOLS $MYTOOLS.runsp*txt 


cd ${SPEC_DIR}
MYTOOLS_ARCHIVE=`find . -name ${MYTOOLS}*tar`

echo "
The built tool for $MYTOOLS is in the archive $MYTOOLS_ARCHIVE
Going to utar it
"

tar -xvf $MYTOOLS_ARCHIVE


cd ${SCRIPT_DIR}
exit 0





