##############################################################################
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN  #
#                                                                            #
# 'reportable' flag not set during run                                       #
# 521.wrf_r (base) did not have enough runs!                                 #
# 554.roms_r (base) did not have enough runs!                                #
# 503.bwaves_r (base) did not have enough runs!                              #
# 508.namd_r (base) did not have enough runs!                                #
# 510.parest_r (base) did not have enough runs!                              #
# 519.lbm_r (base) did not have enough runs!                                 #
# 507.cactuBSSN_r (base) did not have enough runs!                           #
# 538.imagick_r (base) did not have enough runs!                             #
# 526.blender_r (base) did not have enough runs!                             #
# 549.fotonik3d_r (base) did not have enough runs!                           #
# 527.cam4_r (base) did not have enough runs!                                #
# 511.povray_r (base) did not have enough runs!                              #
# 544.nab_r (base) did not have enough runs!                                 #
#                                                                            #
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN  #
##############################################################################
                        SPEC(R) CPU2017 Floating Point Rate Result
                                     My Corporation 

       CPU2017 License: nnn (Your SPEC license number)          Test date: Dec-2017
       Test sponsor: My Corporation                 Hardware availability:         
       Tested by:    My Corporation                 Software availability:         

                       Estimated                       Estimated
                Base     Base       Base        Peak     Peak       Peak
Benchmarks     Copies  Run Time     Rate       Copies  Run Time     Rate 
-------------- ------  ---------  ---------    ------  ---------  ---------   
503.bwaves_r                                NR                                 
507.cactuBSSN_r                              NR                                 
508.namd_r          1        859       1.11  *                                 
510.parest_r        1       1410       1.86  *                                 
511.povray_r        1       1459       1.60  *                                 
519.lbm_r                                   NR                                 
521.wrf_r                                   NR                                 
526.blender_r       1       1041       1.46  *                                 
527.cam4_r                                  NR                                 
538.imagick_r                               NR                                 
544.nab_r                                   NR                                 
549.fotonik3d_r                              NR                                 
554.roms_r                                  NR                                 
==============================================================================
503.bwaves_r                                NR                                 
507.cactuBSSN_r                              NR                                 
508.namd_r          1        859       1.11  *                                 
510.parest_r        1       1410       1.86  *                                 
511.povray_r        1       1459       1.60  *                                 
519.lbm_r                                   NR                                 
521.wrf_r                                   NR                                 
526.blender_r       1       1041       1.46  *                                 
527.cam4_r                                  NR                                 
538.imagick_r                               NR                                 
544.nab_r                                   NR                                 
549.fotonik3d_r                              NR                                 
554.roms_r                                  NR                                 
 Est. SPECrate2017_fp_base             1.48
 Est. SPECrate2017_fp_peak                                          Not Run


                                         HARDWARE
                                         --------
            CPU Name: Intel Core (Broadwell)
            Max MHz.:  
             Nominal:  
             Enabled:  cores, 2 chips,  threads/core
           Orderable:  
            Cache L1:  
                  L2:  
                  L3:  
               Other:  
              Memory: 3.368 GB fixme: If using DDR3, format is:
                      'N GB (M x N GB nRxn PCn-nnnnnR-n, ECC)'
             Storage: 30 GB  add more disk info here
               Other:  


                                         SOFTWARE
                                         --------
                  OS: Scientific Linux CERN SLC release 6.9 (Carbon)
                      3.10.0-514.10.2.el7.x86_64
            Compiler: C/C++/Fortran: Version 6.2.0 of GCC, the
                      GNU Compiler Collection
            Parallel: No
            Firmware:  
         File System: ext4
        System State: Run level N (add definition here)
       Base Pointers: 64-bit
       Peak Pointers: Not Applicable
               Other:  


                                      General Notes
                                      -------------
    Environment variables set by runcpu before the start of the run:
    LD_LIBRARY_PATH = "/usr/lib64/:/usr/lib/:/lib64"
    

                                      Platform Notes
                                      --------------
     Sysinfo program /spec2017/bin/sysinfo
     Rev: r5797 of 2017-06-14 96c45e4568ad54c135fd618bcc091c0f
     running on 4bdabd3c3191 Wed Dec 20 15:54:25 2017
    
     SUT (System Under Test) info as seen by some common utilities.
     For more information on this section, see
        https://www.spec.org/cpu2017/Docs/config.html#sysinfo
    
     From /proc/cpuinfo
        model name : Intel Core Processor (Broadwell)
           2  "physical id"s (chips)
           2 "processors"
        cores, siblings (Caution: counting these is hw and system dependent. The following
        excerpts from /proc/cpuinfo might not be reliable.  Use with caution.)
           cpu cores : 1
           siblings  : 1
           physical 0: cores 0
           physical 1: cores 0
    
     From lscpu:
          Architecture:          x86_64
          CPU op-mode(s):        32-bit, 64-bit
          Byte Order:            Little Endian
          CPU(s):                2
          On-line CPU(s) list:   0,1
          Thread(s) per core:    1
          Core(s) per socket:    1
          Socket(s):             2
          NUMA node(s):          1
          Vendor ID:             GenuineIntel
          CPU family:            6
          Model:                 61
          Model name:            Intel Core Processor (Broadwell)
          Stepping:              2
          CPU MHz:               2194.916
          BogoMIPS:              4389.83
          Virtualization:        VT-x
          Hypervisor vendor:     KVM
          Virtualization type:   full
          L1d cache:             32K
          L1i cache:             32K
          L2 cache:              4096K
          NUMA node0 CPU(s):     0,1
    
     /proc/cpuinfo cache data
        cache size : 4096 KB
    
     From numactl --hardware  WARNING: a numactl 'node' might or might not correspond to a
     physical chip.
    
     From /proc/meminfo
        MemTotal:        3531712 kB
        HugePages_Total:       0
        Hugepagesize:       2048 kB
    
     /usr/bin/lsb_release -d
        Scientific Linux CERN SLC release 6.9 (Carbon)
    
     From /etc/*release* /etc/*version*
        redhat-release: Scientific Linux CERN SLC release 6.9 (Carbon)
        system-release: Scientific Linux CERN SLC release 6.9 (Carbon)
        system-release-cpe: cpe:/o:redhat:enterprise_linux:6.9:ga
    
     uname -a:
        Linux 4bdabd3c3191 3.10.0-514.10.2.el7.x86_64 #1 SMP Fri Mar 3 00:04:05 UTC 2017
        x86_64 x86_64 x86_64 GNU/Linux
    
    
     SPEC is set to: /spec2017
        Filesystem     Type  Size  Used Avail Use% Mounted on
        /dev/vdb       ext4   30G   28G  463M  99% /spec2017
    
     (End of data from sysinfo program)

                                  Compiler Version Notes
                                  ----------------------
    ==============================================================================
     CXXC 508.namd_r(base) 510.parest_r(base)

    ------------------------------------------------------------------------------
    Using built-in specs.
    Target: x86_64-redhat-linux
    Configured with: ../configure --prefix=/usr --mandir=/usr/share/man
      --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla
      --enable-bootstrap --enable-shared --enable-threads=posix
      --enable-checking=release --with-system-zlib --enable-__cxa_atexit
      --disable-libunwind-exceptions --enable-gnu-unique-object
      --enable-languages=c,c++,objc,obj-c++,java,fortran,ada
      --enable-java-awt=gtk --disable-dssi
      --with-java-home=/usr/lib/jvm/java-1.5.0-gcj-1.5.0.0/jre
      --enable-libgcj-multifile --enable-java-maintainer-mode
      --with-ecj-jar=/usr/share/java/eclipse-ecj.jar --disable-libjava-multilib
      --with-ppl --with-cloog --with-tune=generic --with-arch_32=i686
      --build=x86_64-redhat-linux
    Thread model: posix
    gcc version 4.4.7 20120313 (Red Hat 4.4.7-18) (GCC) 
    ------------------------------------------------------------------------------
    
    ==============================================================================
     CC  511.povray_r(base) 526.blender_r(base)

    ------------------------------------------------------------------------------
    Using built-in specs.
    Target: x86_64-redhat-linux
    Configured with: ../configure --prefix=/usr --mandir=/usr/share/man
      --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla
      --enable-bootstrap --enable-shared --enable-threads=posix
      --enable-checking=release --with-system-zlib --enable-__cxa_atexit
      --disable-libunwind-exceptions --enable-gnu-unique-object
      --enable-languages=c,c++,objc,obj-c++,java,fortran,ada
      --enable-java-awt=gtk --disable-dssi
      --with-java-home=/usr/lib/jvm/java-1.5.0-gcj-1.5.0.0/jre
      --enable-libgcj-multifile --enable-java-maintainer-mode
      --with-ecj-jar=/usr/share/java/eclipse-ecj.jar --disable-libjava-multilib
      --with-ppl --with-cloog --with-tune=generic --with-arch_32=i686
      --build=x86_64-redhat-linux
    Thread model: posix
    gcc version 4.4.7 20120313 (Red Hat 4.4.7-18) (GCC) 
    Using built-in specs.
    Target: x86_64-redhat-linux
    Configured with: ../configure --prefix=/usr --mandir=/usr/share/man
      --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla
      --enable-bootstrap --enable-shared --enable-threads=posix
      --enable-checking=release --with-system-zlib --enable-__cxa_atexit
      --disable-libunwind-exceptions --enable-gnu-unique-object
      --enable-languages=c,c++,objc,obj-c++,java,fortran,ada
      --enable-java-awt=gtk --disable-dssi
      --with-java-home=/usr/lib/jvm/java-1.5.0-gcj-1.5.0.0/jre
      --enable-libgcj-multifile --enable-java-maintainer-mode
      --with-ecj-jar=/usr/share/java/eclipse-ecj.jar --disable-libjava-multilib
      --with-ppl --with-cloog --with-tune=generic --with-arch_32=i686
      --build=x86_64-redhat-linux
    Thread model: posix
    gcc version 4.4.7 20120313 (Red Hat 4.4.7-18) (GCC) 
    ------------------------------------------------------------------------------

                                 Base Compiler Invocation
                                 ------------------------
C++ benchmarks: 
     g++

Benchmarks using both C and C++: 
     g++ gcc


                                  Base Portability Flags
                                  ----------------------
    508.namd_r: -DSPEC_LP64
  510.parest_r: -DSPEC_LP64
  511.povray_r: -DSPEC_LP64
 526.blender_r: -funsigned-char -DSPEC_LINUX -DSPEC_LP64


                                 Base Optimization Flags
                                 -----------------------
C++ benchmarks: 
     -m64 -g -O3

Benchmarks using both C and C++: 
     -m64 -std=c99 -g -O3


  SPEC is a registered trademark of the Standard Performance Evaluation
    Corporation.  All other brand and product names appearing in this
    result are trademarks or registered trademarks of their respective
    holders.
##############################################################################
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN  #
#                                                                            #
# 'reportable' flag not set during run                                       #
# 521.wrf_r (base) did not have enough runs!                                 #
# 554.roms_r (base) did not have enough runs!                                #
# 503.bwaves_r (base) did not have enough runs!                              #
# 508.namd_r (base) did not have enough runs!                                #
# 510.parest_r (base) did not have enough runs!                              #
# 519.lbm_r (base) did not have enough runs!                                 #
# 507.cactuBSSN_r (base) did not have enough runs!                           #
# 538.imagick_r (base) did not have enough runs!                             #
# 526.blender_r (base) did not have enough runs!                             #
# 549.fotonik3d_r (base) did not have enough runs!                           #
# 527.cam4_r (base) did not have enough runs!                                #
# 511.povray_r (base) did not have enough runs!                              #
# 544.nab_r (base) did not have enough runs!                                 #
#                                                                            #
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN  #
##############################################################################
------------------------------------------------------------------------------------------
For questions about this result, please contact the tester.
For other inquiries, please contact info@spec.org.
Copyright 2017 Standard Performance Evaluation Corporation
Tested with SPEC CPU2017 v1.0.2 on 2017-12-20 15:54:24+0100.
Report generated on 2017-12-20 17:15:41 by CPU2017 ASCII formatter v5178.
