#!/bin/bash -e

if [[ -z ${CI_PROJECT_DIR} ]]; then
    echo "CI_PROJECT_DIR is not defined. Defining fake params"
    export CI_PROJECT_DIR=$(readlink -f $(dirname $0))/..
    export CI_JOB_NAME="local_test"
    export CI_JOB_ID="noid"
    export CI_COMMIT_SHA='nocommit'
    export CI_REGISTRY_IMAGE="gitlab-registry.cern.ch/hep-benchmarks/hep-spec"
    env | grep "CI_"
fi 
echo "CI_PROJECT_DIR=${CI_PROJECT_DIR}"
echo "CI_JOB_NAME=${CI_JOB_NAME}"
echo "CI_JOB_ID=${CI_JOB_ID}"
echo "CI_COMMIT_SHA=${CI_COMMIT_SHA}"

[[ -z ${BMK_VOLUME} ]] && export BMK_VOLUME=/tmp/${CI_JOB_NAME}_${CI_JOB_ID} 

export BMK_RUNDIR=${BMK_VOLUME}/hep-spec
echo "BMK_VOLUME=${BMK_VOLUME}"
echo "BMK_RUNDIR=${BMK_RUNDIR}"

[[ -z ${SPEC_DIR} ]] && SPEC_DIR=/scratch/HEPSPEC
echo "SPEC_DIR=${SPEC_DIR}"