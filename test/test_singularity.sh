#!/bin/bash

SCRIPT_DIR=$(dirname $0)
source ${SCRIPT_DIR}/config_ci_tests.sh

IMAGE=$CI_REGISTRY_IMAGE/hepspec-cc7-multiarch:qa

# The directory ${BMK_VOLUME} will contain the singularity cache
if [[ -z ${SINGULARITY_CACHEDIR} ]];
then
    export SINGULARITY_CACHEDIR=${BMK_VOLUME}/singularity_cachedir
    [ ! -e ${SINGULARITY_CACHEDIR} ] && mkdir -p ${SINGULARITY_CACHEDIR}
fi

SINGULARITY_IMAGE=${SINGULARITY_IMAGE:-${BMK_VOLUME}/hepspec-cc7.sif}

echo "SINGULARITY_CACHEDIR=${SINGULARITY_CACHEDIR}"
echo "SINGULARITY_IMAGE=${SINGULARITY_IMAGE}"

singularity build -F ${SINGULARITY_IMAGE} docker://${IMAGE} 2> sing_build.err

[[ ! -e ${BMK_VOLUME} ]] && mkdir -p ${BMK_VOLUME}
[[ ! -e ${SPEC_DIR} ]] && mkdir -p ${SPEC_DIR}

echo ${SECRET_URL} | singularity run \
              -B ${BMK_VOLUME}:${BMK_VOLUME} \
              -B ${SPEC_DIR}:${SPEC_DIR} \
              ${SINGULARITY_IMAGE} \
              -w ${BMK_RUNDIR} -b ${BMK} -n 1 -i 1 -p ${SPEC_DIR} -s ${BMK_SET} -u -
