In order to build images a wrapper of kaniko developed at CERN is used: https://gitlab.cern.ch/ci-tools/docker-image-builder
The build image is in general available in the registry gitlab-registry.cern.ch/ci-tools/docker-image-builder:latest

This image is built for x86 architecture

In order to run the same procedure for ARM architecture, and use the same gitlab-ci scripts in our project,
a different image is needed, based on a kaniko image for ARM, with the busybox included (what is called :debug tag)

This image is available at gitlab-registry.cern.ch/hep-benchmarks/hep-spec/docker-image-builder:arm64-v1.3.0
and has been build in the following way on an ARM system

```
git clone https://gitlab.cern.ch/ci-tools/docker-image-builder.git
cd docker-image-builder
mkdir .docker
curl -O https://gitlab.cern.ch/hep-benchmarks/hep-spec/-/raw/master/docker-images/kaniko-cern-arm/Dockerfile_kaniko_cern_arm
docker build -t gitlab-registry.cern.ch/hep-benchmarks/hep-spec/docker-image-builder:arm64-v1.3.0 -f Dockerfile_kaniko_cern_arm .
docker push gitlab-registry.cern.ch/hep-benchmarks/hep-spec/docker-image-builder:arm64-v1.3.0
```
