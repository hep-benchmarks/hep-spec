# HEP SPEC Orchestrator

Orchestrator scripts and docker registry to drive the execution of HEPSPEC06 and SPEC CPU 2017 with the WLCG configuration.

Starting from release [v2.0](https://gitlab.cern.ch/hep-benchmarks/hep-spec/-/releases/v2.0) the images support also ARM CPU
as well as the HS06 and SPEC CPU 2017 configuration file.
The HS06 toolkit needs to be patched, following the instruction in the patch folder [patch_SPEC2006](./patch_SPEC2006/README.md)

## Feedback and Support
Feedback, and support questions are welcome in the HEP Benchmarks Project
[Discourse Forum](https://wlcg-discourse.web.cern.ch/c/hep-benchmarks).
You can also submit issues via 
[Gitlab](https://gitlab.cern.ch/hep-benchmarks/user-support/-/issues).

## Installation

The preferred installation and execution is via a pre-built container.

If root privileges are available, the [install.sh](./install.sh) script can be used to locally install the dependencies needed to run HEPSPEC06 or SPEC CPU 2017

After installation the executable script is located at $HOME/.local/hep-spec/scripts/hep-spec.sh

## Run using the HEP Benchmark Suite

This HEP SPEC orchestrator is integrated within the [HEP Benchamrk Suite](https://gitlab.cern.ch/hep-benchmarks/hep-benchmark-suite).
It can be run installing the suite and using one of the declarative configurations documented in the [example section](https://gitlab.cern.ch/hep-benchmarks/hep-benchmark-suite/#examples).


## Run without the HEP Benchmark Suite

The `-h` option provides an explanation of all command line arguments

### Docker

#### A. pre-installed SPEC tarball 

##### HS06 

###### compile at 32bits
```
#!/usr/bin/env bash
IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-spec/hepspec-cc7-multiarch:latest
# Work directory (change it as preferred)
BIND_VOLUME=/tmp/hep-spec_wd
# HEPSPEC_VOLUME=(where the HEPSPEC has been pre-installed)
HEPSPEC_VOLUME=/var/SPEC

docker run --net=host \
    -v ${BIND_VOLUME}:${BIND_VOLUME} \
    -v ${HEPSPEC_VOLUME}:${HEPSPEC_VOLUME} \
    ${IMAGE} -w ${BIND_VOLUME} -b hs06 -m 32 -p ${HEPSPEC_VOLUME} <other_options>

```

###### compile at 64bits
 
 change in the above command the argument `-m 32` into `-m 64`

##### SPEC CPU2017

```
#!/usr/bin/env bash
IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-spec/hepspec-cc7-multiarch:latest
# Work directory (change it as preferred)
BIND_VOLUME=/tmp/hep-spec_wd
# HEPSPEC_VOLUME=(where the HEPSPEC has been pre-installed)
HEPSPEC_VOLUME=/var/SPEC

docker run --net=host \
    -v ${BIND_VOLUME}:${BIND_VOLUME} \
    -v ${HEPSPEC_VOLUME}:${HEPSPEC_VOLUME} \
    ${IMAGE} -w ${BIND_VOLUME} -b spec2017 -p ${HEPSPEC_VOLUME} <other_options>

```

#### Other options 

- Custom config file:  `-c path_to_config_file` 
   - make sure the config file is in the BIND_VOLUME to be accessible from inside the container
- Custom benchamrk set: ` -s 453.povray`  or `-s 511.povray_r` or `-s intrate` 
   - make sure the bset file is in the location expected by specrun `/benchspec/CPU/`

#### B. Install SPEC from tarball

NB: the tarball can be already available on the system or be accessible from a http URL.
- If it is available on the system, prepend `file:` to the absolute path, i.e. `URL_TARBALL="file:/absolute_path_to_tarball"`
- If it is accessible from a http URL use `URL_TARBALL="https://some_url_to_tarball"`

##### HS06 

###### compile at 32bits
```
#!/usr/bin/env bash
IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-spec/hepspec-cc7-multiarch:latest
# Work directory (change it as preferred)
BIND_VOLUME=/tmp/hep-spec_wd
# HEPSPEC installation dir for unpacking tar
HEPSPEC_VOLUME=${BIND_VOLUME}/SPEC
# URL of the tarball to retrieve
URL_TARBALL="https://something"

docker run --net=host \
    -v ${BIND_VOLUME}:${BIND_VOLUME} \
    -v ${HEPSPEC_VOLUME}:${HEPSPEC_VOLUME} \
    ${IMAGE} -w ${BIND_VOLUME} -b hs06 -m 32 -p ${HEPSPEC_VOLUME} -u ${URL_TARBALL}

```
If that URL_TARBALL must remain secret, the variable can be injected as stdin, the `-u -` should be used.

```
echo ${URL_TARBALL} | docker run -i \
    -v ${BIND_VOLUME}:${BIND_VOLUME} \
    -v ${HEPSPEC_VOLUME}:${HEPSPEC_VOLUME} \
    ${IMAGE} -w ${BIND_VOLUME} -b hs06 -m 32 -p ${HEPSPEC_VOLUME} -u -

```

###### compile at 64bits
 
 change in the above command the argument `-m 32` into `-m 64`

##### SPEC CPU2017 

```
#!/usr/bin/env bash
IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-spec/hepspec-cc7-multiarch:latest
# Work directory (change it as preferred)
BIND_VOLUME=/tmp/hep-spec_wd
# HEPSPEC_VOLUME=(where the HEPSPEC has been pre-installed)
HEPSPEC_VOLUME=/var/SPEC
# location of tarball to retrieve. Put it in the bind volume to make it accessible via docker
URL_TARBALL="file:{BIND_VOLUME}/path_to_tarball"

docker run --net=host \
    -v ${BIND_VOLUME}:${BIND_VOLUME} \
    -v ${HEPSPEC_VOLUME}:${HEPSPEC_VOLUME} \
    ${IMAGE} -w ${BIND_VOLUME} -b spec2017 -p ${HEPSPEC_VOLUME} -u ${URL_TARBALL}

```

### C. Singularity

All the above examples can be adapted for Singularity. Here a single example is provided as guideline
```
#!/usr/bin/env bash
IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-spec/hepspec-cc7-multiarch:latest
BIND_VOLUME=/tmp/hep-spec_wd # writable work directory
mkdir -p ${BIND_VOLUME}
chmod a+w ${BIND_VOLUME}

HEPSPEC_VOLUME=${BIND_VOLUME}/SPEC
mkdir -p ${HEPSPEC_VOLUME}
chmod a+w ${HEPSPEC_VOLUME}

SINGULARITY_CACHEDIR=/tmp/singularity_cachedir
BMK_SET=453.povray # to run a single benchmark

echo ${URL_TARBALL} | singularity run -B ${BIND_VOLUME}:${BIND_VOLUME} \
    -B ${HEPSPEC_VOLUME}:${HEPSPEC_VOLUME} docker://${IMAGE} -w ${BIND_VOLUME} \
    -b hs06 -m32 -p ${HEPSPEC_VOLUME} -s ${BMK_SET} -u -
```
